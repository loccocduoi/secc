TransitionSystem
ADT test

Signature

Sorts

state, var_map, stack, var_name, inst, program, int, temp, label

Generators

//variable map
empty_map : var_map
map: var_name, int, var_map -> var_map

//stack
empty_stack : stack
stk: int, stack -> stack
//pop: stack -> stack
//top: stack -> int

//inst
PUSH: int -> inst
ADDVAR: var_name -> inst
READ: var_name -> inst
WRITE: var_name -> inst
ADD: inst
SUB: inst
MUL: inst
DIV: inst
AND: inst
OR: inst
NOT: inst
EQ: inst
LT: inst
LABEL: label -> inst
GOTO: label -> inst
JUMP: label -> inst

//program
end: program
prog: inst, program -> program

//state
st: program, var_map, stack -> state

//int
zero : int
s : int -> int
m: int -> int
temp: int,int,int->temp //to bypass non linear when rewriting MUL



//variable names
//================================================
tempVar : var_name
insert variables here
//================================================//

//label names
//================================================
insert labels here
//================================================//




Operations
//int operations
plus : int, int -> int
minus : int, int -> int
mul : int, int -> int
//boolean operations
and : int, int -> int
or : int, int -> int
not : int -> int
eq : int, int -> int
lt : int, int -> int

Variables
n : int
m : int
n1 : int
n2 : int
var : var_name
var_map : var_map
stack : stack
program: program
inst : inst
l : label

//================================================
initialState = 
st(
insert initialState here
,empty_map
,empty_stack  )
//================================================//

Strategies

Try(S) = Choice(S, Identity)
ReduceOneSubTerm(V) = Choice(One(ReduceOneSubTerm(V),1),V)
RewriteSetWith(S) = Choice(Choice(Union(S, Not(S)), S), Not(S))
Reduce(S) = Sequence(All(Reduce(S)), S)
ReduceTD(S)= Sequence(S, All(ReduceTD(S)))

//================================================
reloadProg = {st($program, $var_map, $stack) ->
st(
insert initialState here
,$var_map
,$stack )}
//================================================//

//int operations
basicADD = { 
	plus($n, zero) -> $n, 								
	plus(zero, $n) -> $n, 							
	plus(s($n1), s($n2)) -> plus(s(s($n1)),$n2), 	
	plus(s($n1), m($n2)) -> minus(s($n1),$n2), 	
	plus(m($n1), s($n2)) -> minus(s($n2),$n1), 	
	plus(m($n1), m($n2)) -> minus(m($n1),$n2),
	m(zero) -> zero,								
	m(m($n)) -> $n,									
	minus($n, zero) -> $n, 								
	minus(zero, $n) -> m($n), 						
	minus(s($n1), s($n2)) -> minus($n1,$n2), 		
	minus(s($n1), m($n2)) -> plus(s($n1),$n2), 		
	minus(m($n1), s($n2)) -> m(plus($n1,s($n2))), 	
	minus(m($n1), m($n2)) -> minus($n2, $n1) 		
}

simpleMUL = {
	mul($n1,zero) -> zero,
	mul(zero,$n1) -> zero,
	mul($n1, m($n2)) -> minus(zero,mul($n1, $n2)),
	mul(m($n1), $n2) -> minus(zero,mul($n1, $n2))
}

createTempMUL = {
	mul(s($n1), s($n2)) -> temp(zero, zero, mul(s($n1), $n2))
}

increaseTempMUL = {
	temp($n, $m, mul(s($n1), $n2)) -> temp(s($n), s($m), mul($n1,$n2))
}

completeTempMUL = {
	temp($n, $n1, mul(zero, $n2)) -> plus(mul($n1, $n2), $n)
}

complexMUL = Sequence(Sequence(createTempMUL(),Fixpoint(RewriteSetWith(increaseTempMUL()))),completeTempMUL())

basicMUL = Choice(simpleMUL(), complexMUL())

evalArithmetic = Fixpoint(Try(Choice(ReduceOneSubTerm(basicADD()), ReduceOneSubTerm(basicMUL()))))

createArithmeticTerm = {
	st(prog(ADD,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(plus($n2, $n1), $stack)),
	st(prog(SUB,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(minus($n2, $n1), $stack)),
	st(prog(MUL,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(mul($n2, $n1), $stack))
}

rewriteArithmeticTerm = One(One(evalArithmetic(),1),3)

basicBOOL = {
	and(zero,$n) -> zero,
	and($n, zero) -> zero,
	and($n1, $n2) -> s(zero),
	or(zero, zero) -> zero,
	or(zero, $n) -> s(zero),
	or($n, zero) -> s(zero),
	not(zero) -> s(zero),
	not($n) -> zero
}

createBoolTerm = {
	st(prog(AND,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(and($n1, $n2), $stack)),
	st(prog(OR,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(or($n1, $n2), $stack)),
	st(prog(NOT,$program), $var_map, stk($n1, $stack)) -> st($program, $var_map, stk(not($n1), $stack))
}

rewriteBoolTerm = One(One(basicBOOL(),1),3)

basicCOMPARE = {
	eq(zero, zero) -> s(zero),
	eq(s($n1),s($n2)) -> eq($n1, $n2),
	eq(s($n1), m($n2)) -> zero,
	eq(m($n1), m($n2)) -> eq($n1,$n2),
	eq(m($n1), s($n2)) -> zero,
	eq($n1, zero) -> zero,
	eq(zero, $n1) -> zero,
	
	lt(zero, zero) -> zero,
	lt(zero, s($n1)) -> s(zero),
	lt(zero, m($n1)) -> zero,
	lt(s($n1), s($n2)) -> lt($n1, $n2),
	lt(s($n1), zero) -> zero,
	lt(s($n1), m($n2)) -> zero,
	lt(m($n1), s($n2)) -> s(zero),
	lt(m($n1), m($n2)) -> lt($n2, $n1),
	lt(m($n1), zero) -> s(zero)
}

createCompareTerm = {
	st(prog(EQ,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(eq($n2, $n1), $stack)),
	st(prog(LT,$program), $var_map, stk($n1, stk($n2, $stack))) -> st($program, $var_map, stk(lt($n2, $n1), $stack))
}

rewriteCompareTerm = Fixpoint(Try(One(One(basicCOMPARE(),1),3)))


evalPUSH = {st(prog(PUSH($n),$program), $var_map, $stack) -> st($program, $var_map, stk($n, $stack))}

evalADDVAR = {st(prog(ADDVAR($var), $program), $var_map, $stack) -> st($program, map($var, zero, $var_map), $stack)}


createCopyTerm = {map($var, $n, $var_map) -> map(tempVar, zero, map(tempVar, zero, map($var, $n, $var_map)))}
increaseCopyTerm = {
	map(tempVar, $n1, map(tempVar, $n2, map($var, zero, $var_map))) -> map(tempVar, $n1, map(tempVar, $n2, map($var, zero, $var_map))),
	map(tempVar, $n1, map(tempVar, $n2, map($var, m(zero), $var_map))) -> map(tempVar, $n1, map(tempVar, $n2, map($var, m(zero), $var_map))),
	map(tempVar, $n1, map(tempVar, $n2, map($var, s($n), $var_map))) -> map(tempVar, s($n1), map(tempVar, s($n2), map($var, $n, $var_map))),
	map(tempVar, zero, map(tempVar, zero, map($var, m(s($n)), $var_map))) -> map(tempVar, m(s(zero)), map(tempVar, m(s(zero)), map($var, m($n), $var_map))),
	map(tempVar, m($n1), map(tempVar, m($n2), map($var, m(s($n)), $var_map))) -> map(tempVar, m(s($n1)), map(tempVar, m(s($n2)), map($var, m($n), $var_map)))
}
completeCopyTerm = 	{
	map(tempVar, $n1, map(tempVar, $n2, map($var, zero, $var_map))) -> map(tempVar, $n1, map($var, $n2, $var_map)),
	map(tempVar, $n1, map(tempVar, $n2, map($var, m(zero), $var_map))) -> map(tempVar, $n1, map($var, $n2, $var_map))					
}
createCopyVar = Sequence(Sequence(createCopyTerm(),Fixpoint(increaseCopyTerm())),completeCopyTerm())
				
swapUpCopyVar = {map($var, $n, map(tempVar, $n1, $var_map)) -> map(tempVar, $n1, map($var, $n, $var_map))}
swapDownCopyVar = {map(tempVar, $n, map($var, $n1, $var_map)) -> map($var, $n1, map(tempVar, $n, $var_map))}
repeatSwapUp = Choice(swapUpCopyVar(),Sequence(One(repeatSwapUp(),3),swapUpCopyVar()))
putTempVarOnStack = {st($program, map(tempVar, $n, $var_map), $stack) -> st($program, $var_map, stk($n, $stack))}

//consumeREADVAR = {st(prog(READ($var), $program), $var_map, $stack) -> st($program, $var_map, $stack)}
//consumeWRITEVAR = {st(prog(WRITE($var), $program), $var_map, stk($n, $stack)) -> st($program, map(tempVar,$n,$var_map), $stack)}

//variable specific strategies
//================================================
insert var declarations here
//================================================//


consumeInstr = {prog($inst,$program) -> $program}

//label specific strategies
//================================================
insert label declarations here
//================================================//

evalLabel = {st(prog(LABEL($l), $program), $var_map, $stack) -> st($program, $var_map, $stack)}


execPUSH = evalPUSH()
execARITH = Sequence(createArithmeticTerm(), rewriteArithmeticTerm())
execBOOL = Sequence(createBoolTerm(), rewriteBoolTerm())
execCOMPARE = Sequence(createCompareTerm(), rewriteCompareTerm())
execADDVAR = evalADDVAR()
execREADVAR = evalREADVAR()
execWRITEVAR = evalWRITEVAR()
execLABEL = evalLabel()
execGOTO = evalGOTO()
execJUMP = evalJUMP()

Transitions
exec = Fixpoint(Reduce(Choice(Choice(Choice(Choice(Choice(Choice(Choice(Choice(Choice(execPUSH(),execARITH()),
execBOOL()),execCOMPARE()),execADDVAR()),execREADVAR()),execWRITEVAR()),execLABEL()),execJUMP()),Try(execGOTO()))))