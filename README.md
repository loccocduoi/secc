secC, or strataGEM empowered code-verification for C, pronouced "seck-see", is a tool created for verification of C codes.
It accepts a simplified version of C codes, translates the codes into strataGEM transition systems, and generates their state space which can be used to verify properties.

Requirement:
- stratagem 0.5.0 https://sourceforge.net/projects/stratagem-mc/
- pycparser 2.14  https://github.com/eliben/pycparser
- python 2.7 or above