import sys
from os.path import *
from pycparser import c_parser, c_ast, parse_file
from pycparser.c_ast import *
from subprocess import *

ZERO = 'zero'
NOT = '!'
MINUS = '-'
AND = '&&'
OR = '||'
PLUS = '+'
TIMES = '*'
EQUAL = '=='
LESSTHAN = '<'
#SCOPE_DEL = '@'

VAR_MARKER = 'insert variables here\n'
LBL_MARKER = 'insert labels here\n'
PROG_DECL_MARKER = 'insert initialState here'
VAR_DECL_MARKER = 'insert var declarations here'
LBL_DECL_MARKER = 'insert label declarations here'
IF_MARKER = 'insert if strategies here'
IFELSE_MARKER = 'insert ifelse strategies here'
WHILE_MARKER = 'insert dowhile strategies here'

def convertNat(i):
	repeat = int(i)
	if i == 0:
		return ZERO
	return 's('*repeat + ZERO + ')'*repeat


def parse_binop(source):
	inst = ''
	op = source.op
	left = parse_exp(source.left)
	right = parse_exp(source.right)
	if op == PLUS:
		return left + right + ['ADD']
	elif op == MINUS:
		return left + right + ['SUB']
	elif op == TIMES:
		return left + right + ['TIMES']
	elif op == AND:
		return left + right + ['AND']
	elif op == OR:
		return left + right + ['OR']
	elif op == EQUAL:
		return left + right + ['EQ']
	elif op == LESSTHAN:
		return left + right + ['LT']

def parse_unop(source):
	op = source.op
	if op == NOT:
		inst = parse_exp(source.expr) + ['NOT']
	elif op == MINUS:
		inst = ['PUSH(zero)'] + parse_exp(source.expr) + ['SUB']
	return inst

def parse_exp(source):
	if isinstance(source, ID):
		return parse_var(source)
	elif isinstance(source, Constant):
		return ['PUSH(%s)' % convertNat(source.value)]
	elif isinstance(source, BinaryOp):
		return parse_binop(source)
	elif isinstance(source, UnaryOp):
		return parse_unop(source)

#var name as expression
def parse_var(source):
	varName = parse_varname(source)
	return ['READ(%s)' % varName]

#var name as string
def parse_varname(source):
	return '%s' % (source.name)

def parse_dowhile(source):
	global LAST_LABEL
	cond = parse_exp(source.cond)
	block = parse_compound(source.stmt)
	LAST_LABEL += 1
	L1 = 'L%d' %(LAST_LABEL)
	return ['LABEL(%s)' % L1] + block + cond + ['JUMP(%s)' % L1]

def parse_if(source):
	global LAST_LABEL
	cond = parse_exp(source.cond)
	ifTrue = parse_compound(source.iftrue)
	if source.iffalse == None:
		LAST_LABEL += 1
		L1 = 'L%d' %LAST_LABEL
		return cond + ['NOT','JUMP(%s)' % L1] + ifTrue + ['LABEL(%s)' % L1]
	else:
		ifFalse = parse_compound(source.iffalse)
		L1 = 'L%d' %(LAST_LABEL+1)
		L2 = 'L%d' %(LAST_LABEL+2)
		LAST_LABEL += 2
		return cond + ['NOT','JUMP(%s)' % L1] + ifTrue + ['GOTO(%s)' % L2, 'LABEL(%s)' %L1] + ifFalse + ['LABEL(%)' %L2]

def parse_assign(source):
	varName = parse_varname(source.lvalue)
	exp = parse_exp(source.rvalue)
	return exp + ['WRITE(%s)' %varName]

def parse_decl(source):
	varName = parse_varname(source)
	addVar = ['ADDVAR(%s)' % varName]
	init = ['PUSH(zero)']
	writeVar = ['WRITE(%s)' %varName]
	if varName not in VARS:
		VARS.append(varName)
	if source.init != None:
		init = parse_exp(source.init)
	return addVar + init + writeVar

def parse_compound(source):
	prog = []
	cursor = 0
	for node in source.block_items:
		if isinstance(node, Decl):
			prog += parse_decl(node)
		elif isinstance(node, Assignment):
			prog += parse_assign(node)
		elif isinstance(node, If):
			prog += parse_if(node)
		elif isinstance(node, DoWhile):
			prog += parse_dowhile(node)
	return prog
##########################################################################################
if len(sys.argv) < 3:
	print 'usage: %s <input_file> <output_file> [stratagem_path]' % (sys.argv[0])
	sys.exit()
input = sys.argv[1]
output = sys.argv[2]
stratagem = '/Users/loctran/stratagem-0.5.0/bin/stratagem'
if len(sys.argv) > 3:
	stratagem = sys.argv[3]
ast = parse_file(input)
SCOPE = 0
VARS = []
LAST_LABEL = 0

prog = []
cursor = 0
for node in ast.ext:
	if isinstance(node, Decl):
		prog += parse_decl(node)
	elif isinstance(node, FuncDef):
		if node.decl.name == 'main':
			prog += parse_compound(node.body)
		else:
			print 'cannot parse functions other than main()'
iniState = 'end'
for inst in prog:
	iniState = '%sprog(%s,%s)' % (iniState[0:cursor], inst, iniState[cursor:])
	cursor += 6 + len(inst)
print iniState
##########################################################################################
file = open('ins2str.ts')
ins2str = file.read()
file.close()

varLines = ''
VARS = list(set(VARS))
for var in VARS:
	varLines += '%s : var_name\n' %(var)
ins2str = ins2str.replace(VAR_MARKER, varLines)

labelLines = ''
for l in range(1,LAST_LABEL+1):
	labelLines += 'L%d : label\n' %(l)
ins2str = ins2str.replace(LBL_MARKER, labelLines)

ins2str = ins2str.replace(PROG_DECL_MARKER, iniState)

varStrats = '\
consumeREADVAR@= {st(prog(READ(@), $program), $var_map, $stack) -> st($program, $var_map, $stack)}\n\
consumeWRITEVAR@ = {st(prog(WRITE(@), $program), $var_map, stk($n, $stack)) -> st($program, map(tempVar,$n,$var_map), $stack)}\n\
reachVAR@ = {map(@, $n, $var_map) -> map(@, $n, $var_map)}\n\
applyToVAR@(S) = IfThenElse(reachVAR@(),S,One(applyToVAR@(S),3))\n\
readVAR@ = Sequence(applyToVAR@(createCopyVar()),Try(repeatSwapUp()))\n\
evalREADVAR@ = Sequence(Sequence(consumeREADVAR@(), One(readVAR@(),2)),putTempVarOnStack())\n\
beforeVAR@ = {map(tempVar, $n1, map(@, $n2, $var_map)) -> map(tempVar, $n1, map(@, $n2, $var_map))}\n\
applyBeforeVAR@(S) = IfThenElse(beforeVAR@(),S, Sequence(swapDownCopyVar(),One(applyBeforeVAR@(S),3)))\n\
overwriteVAR@ = {map(tempVar, $n1, map(@, $n2, $var_map)) -> map(@, $n1, $var_map)}\n\
writeVAR@ = applyBeforeVAR@(overwriteVAR@())\n\
evalWRITEVAR@ = Sequence(consumeWRITEVAR@(), One(writeVAR@(),2))\n\n'
varLines = ''
evalReadVar = 'evalREADVAR = Fail'
evalWriteVar = 'evalWRITEVAR = Fail'
if len(VARS) > 0:
	evalReadVar = 'evalREADVAR%s()' % (VARS[0])
	evalWriteVar = 'evalWRITEVAR%s()' % (VARS[0])
	for var in VARS[1:]:
		evalReadVar = 'Choice(%s,evalREADVAR%s())' % (evalReadVar, var)
		evalWriteVar = 'Choice(%s,evalWRITEVAR%s())' % (evalWriteVar, var)
	evalReadVar = 'evalREADVAR = ' + evalReadVar
	evalWriteVar = 'evalWRITEVAR = ' + evalWriteVar

for var in VARS:
	varLines += varStrats.replace('@',var)
varLines += evalReadVar + '\n' + evalWriteVar
ins2str = ins2str.replace(VAR_DECL_MARKER, varLines)

lblStrats = '\
consumeGOTOL@ = {st(prog(GOTO(L@), $program), $var_map, $stack) -> st($program, $var_map, $stack)}\n\
consumeJUMPL@ = {st(prog(JUMP(L@), $program), $var_map, stk(zero,$stack)) -> st($program, $var_map, $stack),\n\
			   st(prog(JUMP(L@), $program), $var_map, stk($n,$stack)) -> st(prog(GOTO(L@), $program), $var_map, $stack)}\n\
reachLabelL@ = {prog(LABEL(L@),$program) -> prog(LABEL(L@),$program)}\n\
consumeUntilL@ = IfThenElse(reachLabelL@(), Identity, Sequence(consumeInstr(),consumeUntilL@()))\n\
evalGOTOL@ = Sequence(consumeGOTOL@(), Sequence(reloadProg(),One(consumeUntilL@(),1)))\n\
evalJUMPL@ = Sequence(consumeJUMPL@(), Try(evalGOTOL@()))\n\n'
lblLines = ''
evalGoto = 'evalGOTO = Fail'
evalJump = 'evalJUMP = Fail'
if LAST_LABEL > 0:
	evalGoto = 'evalGOTOL1()'
	evalJump = 'evalJUMPL1()'
	for l in range(2,LAST_LABEL+1):
		evalGoto = 'Choice(%s,evalGOTOL%d())' % (evalGoto,l)
		evalJump = 'Choice(%s,evalJUMPL%d())' % (evalJump,l)
	evalGoto = 'evalGOTO = ' + evalGoto
	evalJump = 'evalJUMP = ' + evalJump

for l in range(1,LAST_LABEL+1):
	lblLines += lblStrats.replace('@',str(l))
lblLines += evalGoto + '\n' + evalJump
ins2str = ins2str.replace(LBL_DECL_MARKER, lblLines)

file = open(output,'w')
file.write(ins2str)
file.close()

command = [stratagem, 'transition-system', '-ss', output]
result = check_output(command).strip().split('\n')
print result[3:7]
print result[-1]
print result[-2]